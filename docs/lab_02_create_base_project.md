# Create Terraform Framework

## Overview

Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

Configuration files describe to Terraform the components needed to run a single application or your entire datacenter. Terraform generates an execution plan describing what it will do to reach the desired state, and then executes it to build the described infrastructure. As the configuration changes, Terraform is able to determine what changed and create incremental execution plans which can be applied.

The infrastructure Terraform can manage includes low-level components such as compute instances, storage, and networking, as well as high-level components such as DNS entries, SaaS features, etc.

Terraform is configured by creating Terraform template files. In these templates you define a number of resources, then Terraform works with its numerous providers to create the resources as described in the templates.

In the following two labs you will create multiple Terraform templates to configure and group resources to provision a [highly-available deployment of the Wordpress content management system](https://github.com/aws-samples/aws-refarch-wordpress).

## Learning Objectives:

- Populate the repository with Terraform code to create base elements (RDS, EFS, ElastiCache)
- Update gitlab-ci.yml to lint and validate the code
- Observe how the pipeline pushes code

## 01 Create Your Infrastructure

To deploy Wordpress in an HA fashion will require the creation of a horizontally scalable fleet of PHP application servers, a distributed filesystem to be shared across those servers, a shared cache for session management, and a shared database for content.

To describe these resources, in the following steps, you will create numerous Terraform templates which logically group together related resources (templates such as `filesystem.tf` or `variables.tf`). These templates, when combined by Terraform, produce a single, complete architecture.

1.  As with any change to your source code begin by creating a new branch in which to work:

    ```bash
    git checkout -b tf_base
    ```

1.  Terraform provisions resources you define through [Providers](https://www.terraform.io/docs/providers/index.html). To require specific versions of these providers create a template that describes the providers you wish to use. To do this, in your project, create `providers.tf` with the following content:

    ```terraform
    provider "aws" {
        region  = var.aws_region
        version = "2.68"
    }

    provider "local" {
        version = "1.4"
    }

    provider "random" {
        version = "3.0.0"
    }

    provider "null" {
        version = "2.1.2"
    }
    ```

1.  You will also want to document the inputs to your Terraform template, the values that can be configured at deployment time to tailor how the resources are provisioned. Declare your inputs as [variables](https://www.terraform.io/docs/configuration/variables.html) in a template named `variables.tf`:

    ```terraform
    ### General
    variable "aws_region" {
        description = "(Required) The region the resources will be provisioned to."
        type = string
    }

    variable "release" {
        description = "Release name"
        type = string
        default = "base"
    }

    ### Database
    variable "rds_instance_type" {
        description = "(Required) The instance type of the RDS instance."
        type = string
        default = "db.m5.large"
    }

    variable "rds_db_identifier" {
        description = "(Optional, Forces new resource) The name of the RDS instance, if omitted, Terraform will assign a random, unique identifier."
        type = string
        default = "wordpress-db"
    }

    variable "db_name" {
        description = "Name of the database"
        default = "wordpressdb"
        type = string
    }

    variable "db_username" {
        description = "Username for the database"
        default = "wpadmin"
        type = string
    }

    variable "db_password" {
        description = "DO NOT CHANGE - Password for the master DB user. Note that this may show up in logs, and it will be stored in the state file."
        default = "380cccf909"
        type = string
    }

    variable "wp_admin" {
        description = "Username for Wordpress admin"
        default = "wpadmin"
        type = string
    }

    variable "wp_password" {
        description = "Default administrator password for the Wordpress admin"
        default = "WpPassword"
        type = string
    }
    ```

1.  When Terraform has completed provisioning the resources described in the templates it can provide outputs to STDOUT. These may be URLs for web servers, hostnames for infrastructure, or any other attributes of the resources you would like to have printed as output. To specify the outputs you are interested in seeing define them as an `output` resource in `outputs.tf`:

    ```terraform
    output "database_endpoint" {
        value = aws_db_instance.wordpress.endpoint
    }
    ```

1.  As Terraform provisions resources it stores metadata about those resources in a state file. This file can be stored locally or, more ideally, in a shared, resilient data store. Configure Terraform to store your state in Amazon S3 by creating the file `backend.tf`:

    ```terraform
    terraform {
        backend "s3" {
            bucket         = "< YOUR TERRAFORM STATE BUCKET FROM GITLAB RUNNER >"
            key            = "project/base/terraform.tfstate"
            region         = "< YOUR TERRAFORM STATE REGION FROM GITLAB RUNNER >"
            dynamodb_table = "< YOUR TERRAFORM LOCK TABLE FROM GITLAB RUNNER >"
            encrypt        = true
        }
    }
    ```

    Be sure to update the `bucket`, `region`, and `dynamodb_table` values with the values that were outputed by deploying the gitlab-runner-tf Terraform code in the previous lab.

1.  Finally create a configuration for the variables of your Terraform code by populating `terraform.tfvars`:

    ```terraform
    aws_region="eu-west-1"
    ```

1.  The structure of your project directory should now look like the following:

    ```
     .
     ├── backend.tf
     ├── outputs.tf
     ├── providers.tf
     ├── terraform.tfvars
     └── variables.tf
    ```

1.  To ensure that your code is correct and well formatted you can execute the following commands from within your terminal:

    ```bash
    terraform init
    terraform fmt
    terraform validate
    ```

    Please note the `validate` command is **expected** to produce an error message. This will be corrected in the next section.

1.  You have now created a semi-complete Terraform project. Check this code into version control to begin managing it like other source code:

    ```bash
    git add *
    git commit -m 'initial commit of terraform code'
    git push --set-upstream origin tf_base
    ```

1.  GitLab will immediately submit the committed code to your GitLab Runner to execute a build of your code based on the `.gitlab-ci.yml` configuration in your project. From the GitLab UI for your project visit **_CI / CD -> Pipelines_** and watch as your CI commands are executed to validate your Terraform code. Like the command above the validation should highlight that the project references resources that don't yet exist.

## 02 Add Resources using Terraform

In the previous section you defined a lot of Terraform resources that will control how Terraform builds your resources. Its now time to provision infrastructure resources to host Wordpress.

In many enterprise organizations it is typical that a function, separate from an application team, provisions an AWS account and environment on behalf of the application team. The application team then build their workload inside of the configured environment. This requires an exchange of information between the application team and the separate function to enable the application to reference the resource IDs for things that were build outside of the application team. Resources like permission policies or roles, subnet and security group IDs, or encryption key IDs. Terraform gives you the ability to reference these values and 'read them in' to your configuration.

The network environment for your Wordpress application has been deployed in advance using an alternative Infrastructure as Code tool, AWS CloudFormation. The security group IDs, subnet IDs, and VPC IDs have all been stored using AWS Parameter Store. To read them in you will now configure Terraform to read these key / value pairs so that they can be referenced later when creating new resources.

1. To auto-detect pre-configured environment values and use them in your Terraform code create a `locals.tf` file:

   ```terraform
   locals {
       vpc_id                    = data.aws_ssm_parameter.vpc_id.value
       default_security_group_id = data.aws_ssm_parameter.vpc_default_sg_id.value
       private_subnets           = split(",", data.aws_ssm_parameter.vpc_private_subnet_ids.value)
       public_subnets            = split(",", data.aws_ssm_parameter.vpc_public_subnet_ids.value)
   }

   data aws_ssm_parameter "vpc_id" {
       name = "VPCId"
   }

   data aws_ssm_parameter "vpc_default_sg_id" {
       name = "DefaultSecurityGroupId"
   }

   data aws_ssm_parameter "vpc_public_subnet_ids" {
       name = "PublicSubnetIds"
   }

   data aws_ssm_parameter "vpc_private_subnet_ids" {
       name = "PrivateSubnetIds"
   }
   ```

1. The next 4 templates are a bit long to be embedded directly in this lab guide. Instead use the links below to view the source for these 4 templates and download them or copy / paste them into your project directory:

   - [filesystem.tf](../src/terraform/filesystem.tf)
   - [cache.tf](../src/terraform/cache.tf)
   - [database.tf](../src/terraform/v1/database.tf)
   - [wordpress.tf](../src/terraform/v1/wordpress.tf)

1. Inspect the contents of these files. Notice that many of them make heavy use of [Terraform Modules](https://www.terraform.io/docs/modules/index.html) that create a desired resource, like a shared filesystem, along with all of the permissions, security groups, and other resources that support the desired resource.

   - `filesystem.tf` uses an [EFS Terraform module](https://registry.terraform.io/modules/cloudposse/efs/aws/latest) to create a shared NFS filesystem using the AWS EFS service.
   - `cache.tf` uses a [Memcached Terraform module](https://registry.terraform.io/modules/cloudposse/elasticache-memcached/aws/latest) to create a shared Memcached cache using the AWS ElastiCache service.
   - `database.tf` creates an HA deployment of MySQL using the [AWS Relational Database Service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance).
   - `wordpress.tf` is mostly a placeholder template which, for now, only creates two AWS Security Groups that are referenced in the preceeding templates.

1. Add these to your repository but do not yet push them to GitLab:

   ```bash
   git add *.tf
   git commit -m 'added infrastructure resources'
   ```

1. Next add additional stages to your GitLab CI configuration to build a plan and deploy the code to AWS. To do this add the following sections to your `.gitlab-ci.yml`:
    ```yaml
    plan-job:
        stage: build
        script:
            - terraform plan -out=$PLAN_FILE -var-file=terraform.tfvars
            - echo Producing JSON version of plan file
            - terraform show --json $PLAN_FILE > $JSON_PLAN_FILE
            - terraform show --json $PLAN_FILE | convert_report > $JSON_PLAN_SUMMARY_FILE
        artifacts:
            paths:
                - $PLAN_FILE
                - $JSON_PLAN_FILE
                - $JSON_PLAN_SUMMARY_FILE
            reports:
                terraform: $JSON_PLAN_SUMMARY_FILE

    # Separate apply job for manual launching Terraform as it can be a destructive action
    apply-job:
        stage: deploy
        environment:
            name: production
        script:
            - terraform apply -input=false $PLAN_FILE
        dependencies:
            - plan-job
        rules:
            - if: '$CI_COMMIT_BRANCH == "master"'
              when: manual
    ```

1. Also update your list of stages to include the two new stages:

    ```yaml
    stages:
        - init
        - validate
        - build
        - deploy
    ```

1. Commit this update to the GitLab configuration and push the change. Your CICD pipeline should validate your code and build a Terraform plan for you to review before manually approving the application of this code to your AWS account.

   ```bash
   git add .gitlab-ci.yml
   git commit -m 'added more stages'
   git push
   ```

1. When the job has validated your code and created a deployment plan it will stop.  

1. At this point you can download and review the deployment plan, in order to review what changes Terraform would make if deployed.  To inspect the change plan click on the **_Passed_** status of the **_Build_** job of your pipeline.

1. On the right, under **_Job Artifacts_** you will see a link to *browse* the outputs of the job.  Click this link.

1. You should see 3 files available for review:

    - `plan.tfplan`, a binary file for use by Terraform itself
    - `tfplan.json`, a JSON document describing the changes that will be made
    - `tfplan_summary.json`, a summary outlining how many resources will be created, modified, or deleted

    Click the link for `tfplan.json` to review the JSON in your browser.

1. If you are happy to proceed with these changes and have them deployed, click **_Merge Requests_** in the left of the GitLab UI.  

1. Click **_Create merge request_** and enter a title for your request, "Merge tested tf_base with master".  Click **_Submit merge request_**.

1. On the resulting review screen, as a maintainer of the repository you can click **_Merge_** and the changes will be imported into the main branch of your project.  The GitLab Runner will detect this merge and automatically build ready for deployment.

1. When the code is ready to be deployed the pipeline will pause, waiting for you to approve the execution of the code.  While viewing the stages of the pipeline in the GitLab UI you will see a triangular *Play*-type button, click this button to release the paused pipeline and allow it to apply your Terraform code to the AWS account.

    > Terraform will create an EFS cluster, a Memcached instance, and a primary and secondary MySQL database via RDS.  As part of this deployment the RDS database will be backed up immediately after its created.  For all of this to complete can take as long as 20 minutes.

---

## Summary

In this lab you created a number of Terraform templates to logically group together the resources needed to prepare for hosting an HA Wordpress installation. You provisioned a central database, a shared filesystem, and a shared cache. You also deployed these resources into a network environment that was provisioned using AWS CloudFormation, another Infrastructure as Code tool, by reading resource identiers from AWS Parameter Store.

In the [last lab](lab_03_deploy_wordpress.md) you will update your Terraform code to deploy virtual machines and configure them using Ansible.
