module "efs" {
  source = "git::https://github.com/cloudposse/terraform-aws-efs.git?ref=tags/0.22.0"

  region          = var.aws_region
  security_groups = [module.wp_sg.this_security_group_id]
  subnets         = local.private_subnets
  vpc_id          = local.vpc_id
  name            = "wordpress-${var.release}"
}
