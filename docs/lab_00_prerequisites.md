# CICD w/ Terraform Workshop Prerequisites

## Overview

To complete this lab first you will need a place in which to work. This lab requires the use of a trusty source code editor (vim, emacs, VS Code, etc) and a terminal for working with some command line tools. For the purposes of a baseline this lab relies upon [AWS Cloud9](https://aws.amazon.com/cloud9/) to provide a Linux-based terminal environment and an IDE for managing and editing source code.

The command line tools involved include [HashiCorp's Terraform](https://www.terraform.io/), [Git](https://git-scm.com/), and [Jq](https://stedolan.github.io/jq/). In this lab you will access your Cloud9 environment and then install the tools needed for the remaining labs.

## Learning Objectives:

- Access EventEngine
- Access & configure Cloud9
- Clone GitLab CI Runner repository

### Assumptions

This lab material is designed to be used in an AWS region with a VPC that was created using [this AWS Cloudformation template](../src/cloudformation/aws_vpc.yaml). This template creates a fairly standard VPC with public and private subnets, and then stores the IDs for security groups, subnets, and the VPC itself into [AWS Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html) values that can be referenced later by Terraform.

This lab material also assumes the commands and code are being executed from within a Cloud9 IDE, however this is not essential - any IDE / code editor / CLI environment with appropriate AWS IAM credentials will suffice. If you **are** running this lab material from an AWS Cloud9 environment you will need permissions to create IAM resources like roles and policies; the Cloud9 managed credentials will not give you this access. Therefore disable the use of managed credentials and ensure that your AWS Cloud9 environment is running with an EC2 Instance Profile with sufficient AWS permissions.

To disable [AWS Cloud9 IDE managed credentials](https://docs.aws.amazon.com/cloud9/latest/user-guide/how-cloud9-with-iam.html#auth-and-access-control-temporary-managed-credentials) open the **_Preferences_** pane for the Cloud9 IDE and disable **_AWS managed credentials_**.

## 01 Access Event Engine

To access a temporary AWS account that has been configured for this event you will use the AWS Event Engine. You will need to sign-in to the Event Engine console and from there get logged into your AWS account.

1.  In order to access your AWS account for this lab visit https://dashboard.eventengine.run
    ![Event Engine Login](imgs/ee-login.png "Event Engine Login Page")
1.  Enter the hash code provided to you by the workshop coordinator and click **_Login_**
1.  From the Event Engine console click THE **_AWS Console_** button and then click **_Open AWS Console_**
    ![Access AWS Console](imgs/ee-console.png "Access AWS Console")

## 02 Access Cloud9

After logging into your AWS account access the [Cloud9 IDE](https://console.aws.amazon.com/cloud9/home) that has been provisioned for you to start preparing your environment for the following labs.

AWS Cloud9 is a cloud-based integrated development environment (IDE) that lets you write, run, and debug your code with just a browser. It includes a code editor, debugger, and terminal. Cloud9 comes prepackaged with essential tools for popular programming languages, including JavaScript, Python, PHP, and more, so you don’t need to install files or configure your development machine to start new projects

After you have accessed your Cloud9 IDE familiarize yourself with the interface and its capabilities. Visit the [Cloud9 documentation](https://docs.aws.amazon.com/cloud9/latest/user-guide/tour-ide.html) for a guided tour.

1.  From the AWS Console homepage type `cloud9` into the text field labeled **_Find Services_**

1.  Select the **Cloud9** recommended option in order to access the [Cloud9 Console](https://console.aws.amazon.com/cloud9/home)
    ![Cloud9 Console Access](imgs/cloud9-access.png)

1.  From the Cloud9 console click the **_Open IDE_** button on the Cloud9 instance tile

1.  You should now be presented with a web-based IDE interface, take a moment to explore the interface.

1.  Before moving to the next step you will need to disable Cloud9 managed AWS credentials. To disable managed credentials open the **_Preferences_** pane for your IDE, and under **_AWS Settings_** on the left, disable **_AWS managed temporary credentials_**.

    ![Cloud9 Disable Managed Credentials](imgs/cloud9-disable.png)

## 03 Clone the GitLab Runner repository

In the next lab you will deploy a GitLab Runner to build the source code you check into your GitLab project. In preparation for that lab, and as a quick exercise of your new Cloud9 IDE, use the terminal enviornment of your Cloud9 IDE to clone the code to deploy a GitLab Runner.

1.  Open a command terminal in the IDE and clone the following GitLab repository:

    ```bash
    git clone https://gitlab.com/jpbarto/gitlab-runner-tf.git
    ```

1.  In the next lab you will configure this Terraform composition to deploy a GitLab Runner to build and execute your CICD pipeline

## 04 Install Terraform & Jq

1.  The code you will develop in this lab requires Terraform v0.12 or higher. HashiCorp Terraform is not pre-installed on Cloud9 so you will need to install it yourself. Use the instructions below to install Terraform from the Cloud9 terminal window.

    ```bash
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
    sudo yum -y install terraform
    ```

    > If you are not using a Cloud9 IDE you can find alternate installation instructions for HashiCorp Terraform [on their website](https://learn.hashicorp.com/terraform/getting-started/install.html).

1.  Lastly the JSON utility `jq` will be required by some of the Terraform code, please ensure it is installed and available.
    ```bash
    sudo yum install -y jq
    ```

---

## Summary

In this lab you accessed your AWS account and ultimately your Cloud9 IDE. You have also prepared the IDE with the tools you will need for the remaining labs.

In the [next lab](lab_01_create_git_repository.md) you will create your first GitLab repository and deploy your GitLab Runner.
